package de.terreon.mushroom.rpc;

public class Notification extends MessageWithId<Notification> {

    public final static int CODE = 1;

    public String method;
    public Object data;

    public Notification setMethod(String method) {
        this.method = method;
        return this;
    }

    public Notification setData(Object data) {
        this.data = data;
        return this;
    }

}
