package de.terreon.mushroom.rpc;

import java.util.HashMap;
import java.util.Map;

public class MethodDispatcher implements Dispatcher {

    public final Object object;
    public final String prefix;
    public final String suffix;
    private final Map<String, Method> methods = new HashMap<>();

    public MethodDispatcher(Object object, String prefix, String suffix) {
        this.object = object;
        this.prefix = prefix;
        this.suffix = suffix;
        //
        for (java.lang.reflect.Method reflectMethod: object.getClass().getMethods()) {
            String methodName = reflectMethod.getName();
            if (!methodName.startsWith(prefix)) {
                continue;
            }
            if (!methodName.endsWith(suffix)) {
                continue;
            }
            methodName = methodName.substring(prefix.length(), methodName.length()-suffix.length());
            if (reflectMethod.getParameterCount() != 1) {
                // TODO log warning
                continue;
            }
            if (methods.put(methodName, new Method(methodName, object, reflectMethod)) != null) {
                throw new RuntimeException("Method name not unique: " + reflectMethod.getName());
            }
        }
    }

    public MethodDispatcher(Object obj, String prefix) {
        this(obj, prefix, "");
    }

    @Override
    public Method getMethod(String methodName) {
        Method method = methods.get(methodName);
        if (method == null) {
            throw new MethodNotFound(methodName);
        }
        return method;
    }

}
