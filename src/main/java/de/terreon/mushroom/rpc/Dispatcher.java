package de.terreon.mushroom.rpc;

public interface Dispatcher {

    Method getMethod(String methodName);

}
