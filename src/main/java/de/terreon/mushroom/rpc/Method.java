package de.terreon.mushroom.rpc;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;

import de.terreon.mushroom.exceptions.MushroomException;

public class Method {

    public final String name;
    public final Object object;
    public final java.lang.reflect.Method method;
    public final Type parameterType;

    public Method(String name, Object object, java.lang.reflect.Method method) {
        this.name = name;
        this.object = object;
        this.method = method;
        this.parameterType = method.getGenericParameterTypes()[0];
    }

    public Object call(Object data) {
        try {
            return this.method.invoke(object, data);
        } catch (IllegalAccessException e) {
            throw new MushroomException(e);
        } catch (IllegalArgumentException e) {
            throw new MushroomException(e);
        } catch (InvocationTargetException e) {
            throw new MushroomException(e.getTargetException());
        }
    }

}
