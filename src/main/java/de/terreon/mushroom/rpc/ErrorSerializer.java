package de.terreon.mushroom.rpc;

import java.lang.reflect.Type;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class ErrorSerializer implements JsonSerializer<Error> {

    @Override
    public JsonElement serialize(Error src, Type typeOfSrc, JsonSerializationContext context) {
        JsonArray array = new JsonArray();
        array.add(Error.CODE);
        array.add(src.messageId);
        array.add(src.request.messageId);
        array.add(context.serialize(src.data));
        return array;
    }

}
