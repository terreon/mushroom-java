package de.terreon.mushroom.rpc;

import java.lang.reflect.Type;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class RequestSerializer implements JsonSerializer<Request> {

    @Override
    public JsonElement serialize(Request src, Type typeOfSrc, JsonSerializationContext context) {
        JsonArray array = new JsonArray();
        array.add(Request.CODE);
        array.add(src.messageId);
        array.add(src.method);
        array.add(context.serialize(src.data));
        return array;
    }

}
