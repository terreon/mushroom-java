package de.terreon.mushroom.rpc;

public abstract class MessageWithId<T extends MessageWithId<T>> extends Message {

    public int messageId;

    @SuppressWarnings("unchecked")
    public T setMessageId(int messageId) {
        this.messageId = messageId;
        return (T) this;
    }

    public int getMessageId() {
        return messageId;
    }

}
