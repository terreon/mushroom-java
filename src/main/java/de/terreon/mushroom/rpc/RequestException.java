package de.terreon.mushroom.rpc;

public class RequestException extends RpcException {

    private static final long serialVersionUID = 1L;

    public Object data;

    public RequestException(Object data) {
        this.data = data;
    }

}
