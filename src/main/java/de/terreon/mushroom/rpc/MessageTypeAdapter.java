package de.terreon.mushroom.rpc;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import de.terreon.mushroom.exceptions.UnexpectedResponse;

public class MessageTypeAdapter extends TypeAdapter<Message> {

    private final Map<Integer, Request> requestMap;
    private final Dispatcher dispatcher;
    private final Gson gson;

    public MessageTypeAdapter(Map<Integer, Request> requestMap, Dispatcher dispatcher, Gson gson) {
        this.requestMap = requestMap;
        this.dispatcher = dispatcher;
        this.gson = gson;
    }

    @Override
    public Message read(JsonReader reader) throws IOException {
        reader.beginArray();
        int code = reader.nextInt();
        Message message;
        if (code == Heartbeat.CODE) {
            message = new Heartbeat()
                    .setLastMessageId(reader.nextInt());
        } else if (code == Notification.CODE) {
            int messageId = reader.nextInt();
            String method = reader.nextString();
            Type dataType = this.dispatcher.getMethod(method).parameterType;
            message = new Notification()
                    .setMessageId(messageId)
                    .setMethod(method)
                    .setData(gson.fromJson(reader, dataType));
        } else if (code == Request.CODE) {
            int messageId = reader.nextInt();
            String method = reader.nextString();
            Type dataType = this.dispatcher.getMethod(method).parameterType;
            message = new Request()
                    .setMessageId(messageId)
                    .setMethod(method)
                    .setData(gson.fromJson(reader, dataType));
        } else if (code == Response.CODE) {
            int messageId = reader.nextInt();
            int requestMessageId = reader.nextInt();
            Request request = requestMap.get(requestMessageId);
            if (request == null) {
                throw new UnexpectedResponse("Unexpected response for message id: " + requestMessageId);
            }
            Object responseData = gson.fromJson(reader, request.responseHandler.responseCallback.parameterType);
            message = new Response()
                    .setMessageId(messageId)
                    .setRequestMessageId(requestMessageId)
                    .setData(responseData);
        } else if (code == Error.CODE) {
            int messageId = reader.nextInt();
            int requestMessageId = reader.nextInt();
            Request request = requestMap.get(requestMessageId);
            Object responseData = gson.fromJson(reader, request.responseHandler.errorCallback.parameterType);
            message = new Error()
                    .setMessageId(messageId)
                    .setRequestMessageId(requestMessageId)
                    .setData(responseData);
        } else if (code == Disconnect.CODE) {
            message = new Disconnect();
        } else {
            throw new RuntimeException("Unexcepted message code: " + code);
        }
        reader.endArray();
        return message;
    }

    @Override
    public void write(JsonWriter writer, Message message) throws IOException {
        throw new RuntimeException("This method should never be called. There are specific type adapters for all concrete subclasses of Message.");
    }

}
