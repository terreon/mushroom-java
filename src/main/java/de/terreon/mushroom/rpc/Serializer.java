package de.terreon.mushroom.rpc;

import java.io.Reader;
import java.io.Writer;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Serializer {

    private final MessageTypeAdapter messageTypeAdapter;
    private final Gson jsonSerializer;
    private final Gson jsonDeserializer;

    /**
     * @param engine Engine This parameter is used to look up the parameter type
     * of {Response} and {Error} messages in the {MessageTypeAdapter}.
     */
    public Serializer(Map<Integer, Request> requestMap, Dispatcher dispatcher, List<TypeAdapterWithType> typeAdapters) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.serializeNulls();
        gsonBuilder.registerTypeAdapter(String.class, new StringTypeAdapter());
        gsonBuilder.registerTypeAdapter(Heartbeat.class, new HeartbeatSerializer());
        gsonBuilder.registerTypeAdapter(Notification.class, new NotificationSerializer());
        gsonBuilder.registerTypeAdapter(Request.class, new RequestSerializer());
        gsonBuilder.registerTypeAdapter(Response.class, new ResponseSerializer());
        gsonBuilder.registerTypeAdapter(Error.class, new ErrorSerializer());
        gsonBuilder.registerTypeAdapter(Disconnect.class, new DisconnectSerializer());
        for (TypeAdapterWithType ta: typeAdapters) {
            gsonBuilder.registerTypeAdapter(ta.type, ta.typeAdapter);
        }
        jsonSerializer = gsonBuilder.create();

        messageTypeAdapter = new MessageTypeAdapter(requestMap, dispatcher, jsonSerializer);
        gsonBuilder.registerTypeAdapter(Message.class, messageTypeAdapter);
        jsonDeserializer = gsonBuilder.create();

    }

    public String serialize(Message message) {
        return jsonSerializer.toJson(message);
    }

    public void serializer(Message message, Writer writer) {
        jsonSerializer.toJson(message, writer);
    }

    public Message deserialize(Reader reader) {
        return jsonDeserializer.fromJson(reader, Message.class);
    }

    public Message deserialize(String message) {
        return jsonDeserializer.fromJson(message, Message.class);
    }

}
