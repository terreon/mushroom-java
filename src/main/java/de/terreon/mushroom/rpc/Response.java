package de.terreon.mushroom.rpc;

public class Response extends MessageWithId<Response> {

    public final static int CODE = 3;

    public int requestMessageId;
    public Object data;

    public Request request;

    public Response setRequestMessageId(int requestMessageId) {
        this.request = null;
        this.requestMessageId = requestMessageId;
        return this;
    }

    public Response setRequest(Request request) {
        this.request = request;
        this.requestMessageId = request.messageId;
        return this;
    }

    public Response setData(Object data) {
        this.data = data;
        return this;
    }

}
