package de.terreon.mushroom.rpc;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.terreon.mushroom.client.ErrorHandler;
import de.terreon.mushroom.exceptions.MushroomException;
import de.terreon.mushroom.http.Transport;

/**
 * Transport neutral message factory and mapper between requests and
 * responses. This is the heart of all RPC handling.
 */
public class Engine implements MessageHandler {

    private final static Logger log = LoggerFactory.getLogger(Engine.class);

    private final Transport transport;
    private final Serializer serializer;
    private final Dispatcher dispatcher;
    private final ErrorHandler errorHandler;
    private final MessageIdGenerator messageIdGenerator = new MessageIdGenerator();
    private final Map<Integer, Request> requests = new HashMap<>();
    private final Executor executor;
    private int lastMessageId = -1;

    public Engine(Transport transport, Dispatcher dispatcher, ErrorHandler errorHandler, Executor executor, List<TypeAdapterWithType> typeAdapters) {
        this.transport = transport;
        transport.setMessageHandler(this);
        this.serializer = new Serializer(requests, dispatcher, typeAdapters);
        this.dispatcher = dispatcher;
        this.errorHandler = errorHandler != null ? errorHandler : new DefaultErrorHandler();
        this.executor = executor;
    }

    private int nextMessageId() {
        return messageIdGenerator.next();
    }

    public void notify(String method, Object data) {
        Notification notification = new Notification()
                .setMessageId(nextMessageId())
                .setMethod(method)
                .setData(data);
        send(notification);
    }

    public void request(String method, Object data, ResponseHandler responseHandler) {
        Request request = new Request()
                .setMessageId(nextMessageId())
                .setMethod(method)
                .setData(data)
                .setResponseHandler(responseHandler);
        requests.put(request.messageId, request);
        send(request);
    }

    public Request getRequest(int messageId) {
        return requests.get(messageId);
    }

    public void send(Message message) {
        transport.send(serializer.serialize(message));
    }

    @Override
    public void onMessage(String messageText) {
        Message message = serializer.deserialize(messageText);
        handleMessage(message);
    }

    public void handleMessage(Message message) {
        if (message instanceof MessageWithId) {
            int messageId = ((MessageWithId<?>) message).getMessageId();
            if (messageId >= lastMessageId) {
                lastMessageId = messageId;
            }
            Heartbeat heartbeat = new Heartbeat()
                        .setLastMessageId(lastMessageId);
            transport.send(serializer.serialize(heartbeat));
        }
        if (message instanceof Heartbeat) {
            handleHeartbeat((Heartbeat) message);
        } else if (message instanceof Notification) {
            handleNotification((Notification) message);
        } else if (message instanceof Request) {
            handleRequest((Request) message);
        } else if (message instanceof Response) {
            handleResponse((Response) message);
        } else if (message instanceof Error) {
            handleError((Error) message);
        } else if (message instanceof Disconnect) {
            handleDisconnect((Disconnect) message);
        } else {
            throw new MushroomException("Unsupported message type: " + message.getClass().getCanonicalName());
        }
    }

    private void handleHeartbeat(Heartbeat message) {
        // FIXME implement unreliable transport feature
    }

    private void handleNotification(Notification message) {
        executor.execute(() -> {
            dispatcher.getMethod(message.method).call(message.data);
        });
    }

    private void handleRequest(Request message) {
        executor.execute(() -> {
            Object responseData = dispatcher.getMethod(message.method).call(message.data);
            send(new Response()
                    .setMessageId(messageIdGenerator.next())
                    .setRequest(message)
                    .setData(responseData));
        });
    }

    private void handleResponse(Response message) {
        Request request = requests.remove(message.requestMessageId);
        if (request == null) {
            log.warn("Ignoring response for unknown request message id: {}", message.requestMessageId);
        }
        executor.execute(() -> {
            request.responseHandler.responseCallback.call(message.data);
        });
    }

    private void handleError(Error message) {
        Request request = requests.remove(message.requestMessageId);
        if (request == null) {
            log.warn("Ignoring error response for unknown request message id: {}", message.requestMessageId);
        }
        executor.execute(() -> {
            request.responseHandler.errorCallback.call(message.data);
        });
    }

    private void handleDisconnect(Disconnect message) {
        transport.disconnect();
    }

    public void onError(Throwable throwable) {
        errorHandler.onError(throwable);
    }

}
