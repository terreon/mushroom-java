package de.terreon.mushroom.rpc;

import java.lang.reflect.Type;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class DisconnectSerializer implements JsonSerializer<Disconnect> {

    @Override
    public JsonElement serialize(Disconnect src, Type typeOfSrc, JsonSerializationContext context) {
        JsonArray array = new JsonArray();
        array.add(Disconnect.CODE);
        return array;
    }

}
