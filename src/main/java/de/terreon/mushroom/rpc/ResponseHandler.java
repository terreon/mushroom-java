package de.terreon.mushroom.rpc;

import de.terreon.mushroom.utils.TypesafeCallback;

public class ResponseHandler {

    public final TypesafeCallback<?> responseCallback;
    public final TypesafeCallback<?> errorCallback;

    public ResponseHandler(TypesafeCallback<?> responseCallback, TypesafeCallback<?> errorCallback) {
        this.responseCallback = responseCallback;
        this.errorCallback = errorCallback;
    }

}
