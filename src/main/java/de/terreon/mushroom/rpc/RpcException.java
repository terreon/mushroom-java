package de.terreon.mushroom.rpc;

import de.terreon.mushroom.exceptions.MushroomException;

public class RpcException extends MushroomException {

    private static final long serialVersionUID = 1L;

    public RpcException() {
        super();
    }

    public RpcException(String message, Throwable cause, boolean causeSupression, boolean writeableStackTrace) {
        super(message, cause, causeSupression, writeableStackTrace);
    }

    public RpcException(String message, Throwable cause) {
        super(message, cause);
    }

    public RpcException(String message) {
        super(message);
    }

    public RpcException(Throwable cause) {
        super(cause);
    }


}
