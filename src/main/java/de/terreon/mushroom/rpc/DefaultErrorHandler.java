package de.terreon.mushroom.rpc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.terreon.mushroom.client.ErrorHandler;

public class DefaultErrorHandler implements ErrorHandler {

    private final Logger log = LoggerFactory.getLogger(DefaultErrorHandler.class);

    @Override
    public void onError(Throwable throwable) {
        log.error("An error occured", throwable);
    }

}
