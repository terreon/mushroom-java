package de.terreon.mushroom.rpc;

public class Error extends MessageWithId<Error> {

    public final static int CODE = 4;

    public int requestMessageId;
    public Object data;

    public Request request;

    public Error setRequestMessageId(int requestMessageId) {
        this.requestMessageId = requestMessageId;
        return this;
    }

    public Error setData(Object data) {
        this.data = data;
        return this;
    }

    public Error setRequest(Request request) {
        this.request = request;
        return this;
    }

}
