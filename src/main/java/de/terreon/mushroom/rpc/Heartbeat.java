package de.terreon.mushroom.rpc;

public class Heartbeat extends Message {

    public final static int CODE = 0;

    public int lastMessageId;

    public Heartbeat setLastMessageId(int lastMessageId) {
        this.lastMessageId = lastMessageId;
        return this;
    }

}
