package de.terreon.mushroom.rpc;

public interface MessageHandler {

    public void onMessage(String message);

}
