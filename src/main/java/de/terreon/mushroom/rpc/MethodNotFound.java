package de.terreon.mushroom.rpc;

public class MethodNotFound extends RpcException {

    private static final long serialVersionUID = 1L;

    public MethodNotFound() {
        super();
    }

    public MethodNotFound(String message, Throwable cause, boolean causeSupression, boolean writeableStackTrace) {
        super(message, cause, causeSupression, writeableStackTrace);
    }

    public MethodNotFound(String message, Throwable cause) {
        super(message, cause);
    }

    public MethodNotFound(String message) {
        super(message);
    }

    public MethodNotFound(Throwable cause) {
        super(cause);
    }

}
