package de.terreon.mushroom.rpc;

public class Request extends MessageWithId<Request> {

    public final static int CODE = 2;

    public String method;
    public Object data;

    public ResponseHandler responseHandler;

    public Request setMethod(String method) {
        this.method = method;
        return this;
    }

    public Request setData(Object data) {
        this.data = data;
        return this;
    }

    public Request setResponseHandler(ResponseHandler responseHandler) {
        this.responseHandler = responseHandler;
        return this;
    }

}
