package de.terreon.mushroom.rpc;

import java.io.IOException;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

/**
 * This fixes Issue 457 of Gson which deserializes empty strings as
 * null values. This was the original behavior of Gson 1.5 but never
 * fixed in newer Gson versions beckause of backwards compatibility.
 *
 * @see https://github.com/google/gson/issues/457
 * @author Michael P. Jung <michael.jung@terreon.de>
 */
public class StringTypeAdapter extends TypeAdapter<String> {

    @Override
    public String read(JsonReader reader) throws IOException {
        if (reader.peek() == JsonToken.NULL) {
            reader.nextNull();
            return null;
        } else {
            return reader.nextString();
        }
    }

    @Override
    public void write(JsonWriter writer, String s) throws IOException {
        writer.value(s);
    }

}
