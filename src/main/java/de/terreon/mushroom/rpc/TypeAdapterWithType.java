package de.terreon.mushroom.rpc;

import java.lang.reflect.Type;

import com.google.gson.TypeAdapter;

public class TypeAdapterWithType {

    public final Type type;
    public final TypeAdapter<?> typeAdapter;

    public TypeAdapterWithType(Type type, TypeAdapter<?> typeAdapter) {
        this.type = type;
        this.typeAdapter = typeAdapter;
    }

}
