package de.terreon.mushroom.rpc;

public class MessageIdGenerator {

    private int lastMessageId = -1;

    public int next() {
        return ++lastMessageId;
    }

}
