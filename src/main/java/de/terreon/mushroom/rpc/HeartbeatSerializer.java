package de.terreon.mushroom.rpc;

import java.lang.reflect.Type;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class HeartbeatSerializer implements JsonSerializer<Heartbeat> {

    @Override
    public JsonElement serialize(Heartbeat src, Type typeOfSrc, JsonSerializationContext context) {
        JsonArray array = new JsonArray();
        array.add(Heartbeat.CODE);
        return array;
    }

}
