package de.terreon.mushroom.rpc;

import java.lang.reflect.Type;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class NotificationSerializer implements JsonSerializer<Notification> {

    @Override
    public JsonElement serialize(Notification src, Type typeOfSrc, JsonSerializationContext context) {
        JsonArray array = new JsonArray();
        array.add(Notification.CODE);
        array.add(src.messageId);
        array.add(src.method);
        array.add(context.serialize(src.data));
        return array;
    }

}
