package de.terreon.mushroom.transport;

import java.util.List;

import de.terreon.mushroom.rpc.Engine;
import de.terreon.mushroom.rpc.Message;

/**
 * Base class for unreliable transports. Unreliable means that
 * the underlying protocol does not guarantee message delivery
 * by itself. This is typically the case for any protocol that
 * does raw communication without a layer that guarantees message
 * delivery by itself.
 */
public class UnreliableTransport {

    public List<Message> message;
    public int lastMessageId;
    public Engine engine;
    public String remoteAddr;
    public State state;

    // FIXME implement

}
