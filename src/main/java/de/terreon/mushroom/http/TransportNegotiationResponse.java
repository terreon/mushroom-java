package de.terreon.mushroom.http;

import java.net.URI;

public class TransportNegotiationResponse {

    public URI url;
    public String transport;

}
