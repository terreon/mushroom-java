package de.terreon.mushroom.http;

import java.util.ArrayList;
import java.util.List;

public class TransportNegotiationRequest {

    public List<String> transports = new ArrayList<>();
    public Object auth;

    public TransportNegotiationRequest addTransport(String transport) {
        this.transports.add(transport);
        return this;
    }

    public TransportNegotiationRequest setAuth(Object auth) {
        this.auth = auth;
        return this;
    }

}
