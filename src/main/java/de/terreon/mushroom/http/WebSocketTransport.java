package de.terreon.mushroom.http;

import java.io.IOException;
import java.net.URI;

import javax.websocket.ClientEndpoint;
import javax.websocket.CloseReason;
import javax.websocket.ContainerProvider;
import javax.websocket.DeploymentException;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.terreon.mushroom.client.ErrorHandler;
import de.terreon.mushroom.client.SessionHandler;
import de.terreon.mushroom.exceptions.MushroomException;
import de.terreon.mushroom.exceptions.MushroomIOException;
import de.terreon.mushroom.rpc.MessageHandler;

@ClientEndpoint
public class WebSocketTransport implements Transport {

    private static final Logger log = LoggerFactory.getLogger(WebSocketTransport.class);

    private final SessionHandler sessionHandler;
    private final ErrorHandler errorHandler;
    private final URI transportURI;
    private final WebSocketContainer wsContainer;

    private MessageHandler messageHandler;
    private Session session;

    public WebSocketTransport(URI uri, SessionHandler sessionHandler, ErrorHandler errorHandler) {
        this.transportURI = uri;
        this.sessionHandler = sessionHandler;
        this.errorHandler = errorHandler;
        this.wsContainer = ContainerProvider.getWebSocketContainer();
    }

    @Override
    public void setMessageHandler(MessageHandler messageHandler) {
        if (messageHandler == null) {
            throw new NullPointerException("messageHandler must not be null");
        }
        if (this.messageHandler != null) {
            throw new RuntimeException("MessageHandler already set.");
        }
        this.messageHandler = messageHandler;
    }

    @Override
    public void connect() {
        // FIXME the following code should be made async
        try {
            wsContainer.connectToServer(this, transportURI);
        } catch (DeploymentException e) {
            throw new MushroomException(e);
        } catch (IOException e) {
            throw new MushroomException(e);
        }
    }

    @Override
    public void disconnect() {
        try {
            session.close();
        } catch (IOException e) {
            throw new MushroomIOException(e);
        }
    }

    @Override
    public void send(String message) {
        log.debug("Sending: " + message);
        try {
            session.getBasicRemote().sendText(message);
        } catch (IOException e) {
            throw new MushroomIOException();
        }
    }

    @OnOpen
    public void onOpen(Session session, EndpointConfig config) {
        log.debug("Session opened");
        this.session = session;
        try {
            sessionHandler.onConnect();
        } catch (Exception e) {
            errorHandler.onError(e);
        }
    }

    @OnClose
    public void onClose(Session session, CloseReason reason) {
        log.debug("Session closed (reason: {})", reason);
        this.session = null;
        try {
            sessionHandler.onDisconnect();
        } catch (Exception e) {
            errorHandler.onError(e);
        }
    }

    @OnMessage
    public void onMessage(String message) {
        log.debug("Received: {}", message);
        try {
            messageHandler.onMessage(message);
        } catch (Exception e) {
            errorHandler.onError(e);
        }
    }

    @OnError
    public void onError(Session session, Throwable throwable) {
        errorHandler.onError(throwable);
    }

}
