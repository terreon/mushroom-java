package de.terreon.mushroom.http;

public class SimpleAuth {

    String email;
    String password;

    public SimpleAuth setEmail(String email) {
        this.email = email;
        return this;
    }

    public SimpleAuth setPassword(String password) {
        this.password = password;
        return this;
    }

}
