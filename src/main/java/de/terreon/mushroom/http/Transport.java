package de.terreon.mushroom.http;

import de.terreon.mushroom.rpc.MessageHandler;

public interface Transport {

    public void connect();

    public void disconnect();

    public void setMessageHandler(MessageHandler messageHandler);

    public void send(String serialize);

}
