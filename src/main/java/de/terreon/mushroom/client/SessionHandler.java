package de.terreon.mushroom.client;

public interface SessionHandler {

    public void onConnect();

    public void onDisconnect();

}
