package de.terreon.mushroom.client;

public interface ResponseListener<T, E> {

    public void onResponse(T data);

    public void onError(E data);

}
