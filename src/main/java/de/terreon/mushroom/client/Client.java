package de.terreon.mushroom.client;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URI;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.concurrent.Executor;

import org.apache.commons.io.IOUtils;

import com.google.gson.Gson;

import de.terreon.mushroom.exceptions.AuthenticationError;
import de.terreon.mushroom.exceptions.MushroomException;
import de.terreon.mushroom.exceptions.MushroomIOException;
import de.terreon.mushroom.exceptions.TransportNegotiationError;
import de.terreon.mushroom.http.Transport;
import de.terreon.mushroom.http.TransportNegotiationRequest;
import de.terreon.mushroom.http.TransportNegotiationResponse;
import de.terreon.mushroom.http.WebSocketTransport;
import de.terreon.mushroom.rpc.Dispatcher;
import de.terreon.mushroom.rpc.Engine;
import de.terreon.mushroom.rpc.ResponseHandler;
import de.terreon.mushroom.rpc.TypeAdapterWithType;
import de.terreon.mushroom.utils.Callback;
import de.terreon.mushroom.utils.ErrorHandlerWithExecutor;
import de.terreon.mushroom.utils.SameThreadExecutor;
import de.terreon.mushroom.utils.SessionHandlerWithExecutor;
import de.terreon.mushroom.utils.TypesafeCallback;

public class Client {

    private final URL serverUrl;
    private final Object authObject;
    private final Dispatcher dispatcher;
    private final SessionHandler sessionHandler;
    private final ErrorHandler errorHandler;
    private final Executor executor;
    private final List<TypeAdapterWithType> extraTypeAdapters;

    private Transport transport;
    private Engine engine;

    public Client(URL serverUrl, Object authObject, Dispatcher dispatcher, SessionHandler sessionHandler, ErrorHandler errorHandler, Executor executor, List<TypeAdapterWithType> extraTypeAdapters) {
        this.serverUrl = serverUrl;
        this.authObject = authObject;
        this.dispatcher = dispatcher;
        this.sessionHandler = new SessionHandlerWithExecutor(sessionHandler, executor);
        this.errorHandler = new ErrorHandlerWithExecutor(errorHandler, executor);
        this.executor = executor;
        this.extraTypeAdapters = extraTypeAdapters;
    }

    public Client(URL serverUrl, Object authObject, Dispatcher dispatcher, SessionHandler sessionHandler, ErrorHandler errorHandler) {
        this(serverUrl, authObject, dispatcher, sessionHandler, errorHandler, new SameThreadExecutor(), null);
    }

    public Client(URL serverUrl, Object authObject, Dispatcher dispatcher, SessionHandler sessionHandler) {
        this(serverUrl, authObject, dispatcher, sessionHandler, null);
    }

    public Client(String serverUrl, Object authObject, Dispatcher dispatcher, SessionHandler sessionHandler, ErrorHandler errorHandler) {
        this(parseUrl(serverUrl), authObject, dispatcher, sessionHandler, errorHandler);
    }

    public Client(String serverUrl, Object authObject, Dispatcher dispatcher, SessionHandler sessionHandler) {
        this(serverUrl, authObject, dispatcher, sessionHandler, null);
    }

    private static URL parseUrl(String url) {
        try {
            return new URL(url);
        } catch (MalformedURLException e) {
            throw new MushroomException(e);
        }
    }

    public void connect() {
        // FIXME the following code should be made async
        negotiateTransport();
        this.engine = new Engine(transport, dispatcher, errorHandler, executor, extraTypeAdapters);
        transport.connect();
    }

    private void negotiateTransport() {
        HttpURLConnection conn;
        try {
            conn = (HttpURLConnection) serverUrl.openConnection();
        } catch (IOException e) {
            throw new MushroomIOException(e);
        }
        try {
            conn.setRequestMethod("POST");
        } catch (ProtocolException e) {
            throw new MushroomException(e);
        }
        conn.setDoOutput(true);

        Gson gson = new Gson();
        TransportNegotiationRequest request = new TransportNegotiationRequest()
                .addTransport("ws")
                .setAuth(authObject);
        TransportNegotiationResponse response;

        try {
            Writer wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(gson.toJson(request));
            wr.close();
            if (conn.getResponseCode() == 200) {
                InputStreamReader rd = new InputStreamReader(conn.getInputStream());
                response = gson.fromJson(rd, TransportNegotiationResponse.class);
                rd.close();
            } else {
                InputStream is;
                try {
                    is = conn.getInputStream();
                } catch (IOException e) {
                    is = conn.getErrorStream();
                }
                String error = null;
                if (is != null) {
                    error = IOUtils.toString(is, StandardCharsets.UTF_8);
                    is.close();
                }
                if (conn.getResponseCode() == 401) {
                    throw new AuthenticationError(error);
                } else {
                    throw new TransportNegotiationError("Server replied with unsupported HTTP status code: " + conn.getResponseMessage());
                }
            }
        } catch (IOException e) {
            throw new MushroomIOException(e);
        }
        conn.disconnect();

        if (response.transport.equals("ws")) {
            URI transportURI = response.url;
            transport = new WebSocketTransport(transportURI, sessionHandler, errorHandler);
        } else {
            throw new MushroomException("Unsupported transport: " + response.transport);
        }
    }

    public <ResponseData, ErrorData> void request(String method, Object data, ResponseHandler responseHandler) {
        engine.request(method, data, responseHandler);
    }

    public <ResponseData, ErrorData> void request(
            String method, Object data,
            TypesafeCallback<ResponseData> responseCallback,
            TypesafeCallback<ErrorData> errorCallback) {
        request(method, data, new ResponseHandler(responseCallback, errorCallback));
    }

    public <ResponseData, ErrorData> void request(
            String method, Object data,
            Class<ResponseData> responseClass,
            Callback<ResponseData> responseCallback,
            Class<ErrorData> errorClass,
            Callback<ErrorData> errorCallback) {
        request(method, data,
                new TypesafeCallback<ResponseData>(responseClass, responseCallback),
                new TypesafeCallback<ErrorData>(errorClass, errorCallback));
    }

}
