package de.terreon.mushroom.client;

public interface ErrorHandler {

    public void onError(Throwable throwable);

}
