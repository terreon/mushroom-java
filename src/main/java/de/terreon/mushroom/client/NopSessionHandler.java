package de.terreon.mushroom.client;

public class NopSessionHandler implements SessionHandler {

    @Override
    public void onConnect() {}

    @Override
    public void onDisconnect() {}

}
