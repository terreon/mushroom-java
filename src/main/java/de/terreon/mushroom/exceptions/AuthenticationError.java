package de.terreon.mushroom.exceptions;

public class AuthenticationError extends MushroomException {

    private static final long serialVersionUID = 1L;

    public AuthenticationError() {
        super();
    }

    public AuthenticationError(String message, Throwable cause, boolean causeSupression, boolean writeableStackTrace) {
        super(message, cause, causeSupression, writeableStackTrace);
    }

    public AuthenticationError(String message, Throwable cause) {
        super(message, cause);
    }

    public AuthenticationError(String message) {
        super(message);
    }

    public AuthenticationError(Throwable cause) {
        super(cause);
    }

}
