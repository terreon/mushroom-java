package de.terreon.mushroom.exceptions;

public class MushroomIOException extends MushroomException {

    private static final long serialVersionUID = 1L;

    public MushroomIOException() {
        super();
    }

    public MushroomIOException(String message, Throwable cause, boolean causeSupression, boolean writeableStackTrace) {
        super(message, cause, causeSupression, writeableStackTrace);
    }

    public MushroomIOException(String message, Throwable cause) {
        super(message, cause);
    }

    public MushroomIOException(String message) {
        super(message);
    }

    public MushroomIOException(Throwable cause) {
        super(cause);
    }

}
