package de.terreon.mushroom.exceptions;

public class UnexpectedResponse extends MushroomException {

    private static final long serialVersionUID = 1L;

    public UnexpectedResponse() {
        super();
    }

    public UnexpectedResponse(String message, Throwable cause, boolean causeSupression, boolean writeableStackTrace) {
        super(message, cause, causeSupression, writeableStackTrace);
    }

    public UnexpectedResponse(String message, Throwable cause) {
        super(message, cause);
    }

    public UnexpectedResponse(String message) {
        super(message);
    }

    public UnexpectedResponse(Throwable cause) {
        super(cause);
    }
}
