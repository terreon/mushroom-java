package de.terreon.mushroom.exceptions;

public class MushroomException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public MushroomException() {
        super();
    }

    public MushroomException(String message, Throwable cause, boolean causeSupression, boolean writeableStackTrace) {
        super(message, cause, causeSupression, writeableStackTrace);
    }

    public MushroomException(String message, Throwable cause) {
        super(message, cause);
    }

    public MushroomException(String message) {
        super(message);
    }

    public MushroomException(Throwable cause) {
        super(cause);
    }

}
