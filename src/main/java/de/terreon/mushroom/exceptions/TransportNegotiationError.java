package de.terreon.mushroom.exceptions;

public class TransportNegotiationError extends MushroomException {

    private static final long serialVersionUID = 1L;

    public TransportNegotiationError() {
        super();
    }

    public TransportNegotiationError(String message, Throwable cause, boolean causeSupression, boolean writeableStackTrace) {
        super(message, cause, causeSupression, writeableStackTrace);
    }

    public TransportNegotiationError(String message, Throwable cause) {
        super(message, cause);
    }

    public TransportNegotiationError(String message) {
        super(message);
    }

    public TransportNegotiationError(Throwable cause) {
        super(cause);
    }

}
