package de.terreon.mushroom.utils;

import java.lang.reflect.Type;

import com.google.gson.reflect.TypeToken;

public class TypesafeCallback<T> implements Callback<Object> {

    public final Class<T> parameterClass;
    public final Type parameterType;
    private final Callback<T> callback;

    public TypesafeCallback(Class<T> parameterClass, Callback<T> callback) {
        this.parameterClass = parameterClass;
        this.parameterType = TypeToken.get(parameterClass).getType();
        this.callback = callback;
    }

    @Override
    public void call(Object data) {
        callback.call(parameterClass.cast(data));
    }

}
