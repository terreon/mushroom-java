package de.terreon.mushroom.utils;

import java.util.concurrent.Executor;

import de.terreon.mushroom.client.ErrorHandler;

public class ErrorHandlerWithExecutor implements ErrorHandler {

    private ErrorHandler errorHandler;
    private Executor executor;

    public ErrorHandlerWithExecutor(ErrorHandler errorHandler, Executor executor) {
        this.errorHandler = errorHandler;
        this.executor = executor;
    }

    @Override
    public void onError(Throwable throwable) {
        executor.execute(() -> {
            errorHandler.onError(throwable);
        });
    }

}
