package de.terreon.mushroom.utils;

import java.util.concurrent.Executor;

import javax.swing.SwingUtilities;

public class SwingThreadExecutor implements Executor {

    @Override
    public void execute(Runnable command) {
        SwingUtilities.invokeLater(command);
    }

}
