package de.terreon.mushroom.utils;

import java.util.concurrent.Executor;

import de.terreon.mushroom.client.SessionHandler;

public class SessionHandlerWithExecutor implements SessionHandler {

    private SessionHandler sessionHandler;
    private Executor executor;

    public SessionHandlerWithExecutor(SessionHandler sessionHandler, Executor executor) {
        this.sessionHandler = sessionHandler;
        this.executor = executor;
    }

    @Override
    public void onConnect() {
        executor.execute(() -> {
            sessionHandler.onConnect();
        });
    }

    @Override
    public void onDisconnect() {
        executor.execute(() -> {
            sessionHandler.onDisconnect();
        });
    }

}
