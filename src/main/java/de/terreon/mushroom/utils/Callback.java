package de.terreon.mushroom.utils;

public interface Callback<T> {

    public void call(T obj);

}
