package de.terreon.mushroom.utils;

import java.util.concurrent.Executor;

/**
 * Simple <code>java.util.concurrent.Executor</code> implementation which
 * simply uses the current thread. It is used in the client when there is
 * no executor specified.
 * @author Michael P. Jung <michael.jung@terreon.de>
 */
public class SameThreadExecutor implements Executor {

    @Override
    public void execute(Runnable command) {
        command.run();
    }

}
