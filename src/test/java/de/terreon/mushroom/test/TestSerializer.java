package de.terreon.mushroom.test;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import de.terreon.mushroom.rpc.Dispatcher;
import de.terreon.mushroom.rpc.Heartbeat;
import de.terreon.mushroom.rpc.Message;
import de.terreon.mushroom.rpc.MethodDispatcher;
import de.terreon.mushroom.rpc.Notification;
import de.terreon.mushroom.rpc.Request;
import de.terreon.mushroom.rpc.Response;
import de.terreon.mushroom.rpc.ResponseHandler;
import de.terreon.mushroom.rpc.Serializer;
import de.terreon.mushroom.utils.TypesafeCallback;

public class TestSerializer {

    private Map<Integer, Request> requestMap;
    private Dispatcher dispatcher;
    private Serializer serializer;

    public class MyMessage {
        public String s;
        public Integer i;
    }

    @Before
    public void setUp() throws Exception {
        requestMap = new HashMap<>();
        dispatcher = new MethodDispatcher(this, "rpc_");
        serializer = new Serializer(requestMap, dispatcher);
    }

    @Test
    public void testEmptyString() {
        String messageJson = "[1, 0, \"string\", \"\"]";
        Message message = serializer.deserialize(messageJson);
        assertEquals("", ((Notification) message).data);
    }

    @Test
    public void testDeserializeHeartbeat() {
        String messageJson = "[0, 5]";
        Heartbeat heartbeat = (Heartbeat) serializer.deserialize(messageJson);
        assertEquals(5, heartbeat.lastMessageId);
    }

    @Test
    public void testDeserializeNotification() {
        String messageJson = "[1, 13, \"int\", 42]";
        Notification notification = (Notification) serializer.deserialize(messageJson);
        assertEquals(13, notification.messageId);
        assertEquals("int", notification.method);
        assertEquals(42, notification.data);
    }

    @Test
    public void testDeserializeRequest() {
        String messageJson = "[2, 15, \"timestwo\", 8]";
        Request request = (Request) serializer.deserialize(messageJson);
        assertEquals(15, request.messageId);
        assertEquals("timestwo", request.method);
        assertEquals(8, request.data);
    }

    @Test
    public void testDeserializeResponse() {
        requestMap.put(15, new Request()
                .setMessageId(15)
                .setMethod("timestwo")
                .setResponseHandler(new ResponseHandler(
                        new TypesafeCallback<>(int.class, response -> {}),
                        new TypesafeCallback<>(String.class, error -> {}))));
        String messageJson = "[3, 18, 15, 16]";
        Response response = (Response) serializer.deserialize(messageJson);
        assertEquals(18, response.messageId);
        assertEquals(15, response.requestMessageId);
        assertEquals(16, response.data);
    }

    @Test
    public void testDeserializeNullString() {
        requestMap.put(18, new Request()
                .setMessageId(18)
                .setMethod("nullstring")
                .setResponseHandler(new ResponseHandler(
                        new TypesafeCallback<>(MyMessage.class, response -> {}),
                        new TypesafeCallback<>(String.class, error -> {}))));
        String messageJson = "[3, 20, 18, {s: null, i:null}]";
        Response response = (Response) serializer.deserialize(messageJson);
        assertEquals(20, response.messageId);
        assertEquals(null, ((MyMessage)response.data).i);
        assertEquals(null, ((MyMessage)response.data).s);
    }

    public void rpc_string(String s) {}
    public void rpc_int(int i) {}
    public int rpc_timestwo(int i) { return i * 2; }

}
