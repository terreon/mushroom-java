package de.terreon.mushroom.test;

import org.junit.Test;
import static org.junit.Assert.*;

import de.terreon.mushroom.rpc.Dispatcher;
import de.terreon.mushroom.rpc.MethodDispatcher;

public class TestMethodDispatcher {

    public class MethodsWithPrefix {
        public String rpc_echo(String data) {
            return data;
        }
    }

    public class MethodsWithSuffix {
        public String echo_rpc(String data) {
            return data;
        }
    }

    @Test
    public void testPrefix() {
        Dispatcher dispatcher = new MethodDispatcher(new MethodsWithPrefix(), "rpc_");
        assertEquals(dispatcher.getMethod("echo").call("maltron"), "maltron");
    }

    @Test
    public void testSuffix() {
        Dispatcher dispatcher = new MethodDispatcher(new MethodsWithSuffix(), "", "_rpc");
        assertEquals(dispatcher.getMethod("echo").call("maltron"), "maltron");
    }

}
